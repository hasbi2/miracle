import React, { Component } from "react";
import { BrowserRouter, Route, Switch, useLocation, } from "react-router-dom";
import "./scss/style.scss";
import aixpTracker from "aixp-analytics";
import {
  isMobile
} from "react-device-detect";

aixpTracker.initialize({
  AIXP: {
    scope: "787543b8-6337-4013-a0de-19bf58a72ab6",//aixp
    url: "https://api.digitallab.id/831550f1-976d-407e-915b-2d0f9b7f2e16",//aixp
    // scope: "39ac9fa6-6321-4170-a7be-bd8cad05212a",//local
    // url: "http://localhost:4000/11111111-1111-1111-1111-111111111111",//local
  }
});
aixpTracker.identify(
  {
  },
  {},
  () => {
    console.log("initial identify is sent");
  }
);
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// Containers
const TheLayout = React.lazy(() => import("./containers/TheLayout"));

// Pages
const Login = React.lazy(() => import("./views/pages/login/Login"));
const Register = React.lazy(() => import("./views/pages/register/Register"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));
const Page500 = React.lazy(() => import("./views/pages/page500/Page500"));

const App = () => {
  const location = useLocation()
  React.useEffect(() => {
    aixpTracker.page()
  }, [location])

  return (
    <React.Suspense fallback={loading}>
      <Switch>
        <Route
          exact
          path="/login"
          name="Login Page"
          render={(props) => <Login {...props} />}
        />
        <Route
          exact
          path="/register"
          name="Register Page"
          render={(props) => <Register {...props} />}
        />
        <Route
          exact
          path="/404"
          name="Page 404"
          render={(props) => <Page404 {...props} />}
        />
        <Route
          exact
          path="/500"
          name="Page 500"
          render={(props) => <Page500 {...props} />}
        />
        <Route
          path="/"
          name="Home"
          render={(props) => <TheLayout {...props} />}
        />
      </Switch>
    </React.Suspense>
  );
}

export default () => (
  <BrowserRouter>
    <App />
  </BrowserRouter>
)
